# Manage opening hours with ease!

# Install

1) Add this in your `INSTALLED_APPS`:
    ```py
    "nested_admin",
    "opening_hours_management",
    ```

# Use

1) Import module in your models.py:
    ```py
    from opening_hours_management.models import OpeningHours
    ```
2) Add your opening attribute in your model:
    ```py
    opening = models.OneToOneField(
        OpeningHours,
        verbose_name=_("Opening hours"),
        on_delete=models.SET_NULL,
        null=True,
    )
    ```
3) Run `makemigrations` and `migrate`.
4) Add this on your template file:
    ```djhtml
    {% load opening_hours_management %}

    {% show_opening_hours object.opening <True/False> %}
    ```
    Here `object.opening` is our `OpeningHours` OneToOne, and `True/False` is a condition that makes the templatetag displays special openings/closings that have already occurred. It defaults to `False` if not present ([example](https://gitlab.com/kapt/open-source/django-opening-hours-management/uploads/0e44c4848c5328c4ea26a090ab156d20/image.png)). 
5) ![That's all folks!](https://gitlab.com/kapt/modules/kapt-meta/uploads/518f52e88c5aec013b71204845a41c6a/image.png)



----

# Screenshot

![Screenshot of django-opening-hours-management](https://gitlab.com/kapt/open-source/django-opening-hours-management/uploads/16d8e3329a6ff5b98e0a10e2cf5cddb7/image.png)