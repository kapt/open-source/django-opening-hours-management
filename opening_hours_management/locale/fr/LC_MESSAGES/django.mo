��    +      t  ;   �      �     �     �     �  <   �                %     6     M  )   i  '   �     �     �     �  	   �     �     �               -     >     G     ]     x     �     �     �  A   �  6     '   >  0   f  <   �     �     �     �  	   �     �     �     
          .  	   D  A  N     �     �     �  H   �     	     	     	     !	  $   0	  7   U	  1   �	     �	  !   �	     �	     �	     �	      
     0
     B
     X
     r
     y
  *   �
     �
     �
  +   �
     '  E   0  F   v  8   �  >   �  @   5     v     |          �     �     �     �     �     �                 &       %                +   #   "      '          
          )                                          (                          *      !                                                 	   $    Closed Closing time Closure reason Closure reason is used only if the field "Closed" is checked Friday From General holidays General holidays hours General holidays time range If checked, prevails on time range values If checked, prevails on week day values Monday No opening hours defined Open Open 24/7 Opening hours Opening hours management Opening time Openings hours Popup closing... Saturday Specific period hours Specific period time range Specific periods Specific periods hours Specific periods times ranges Sunday The form cannot be validated because some periods are in conflict The opening time cannot be later than the closing time The start and end times cannot be equal The start date cannot be later than the end date This period is in conflict with the period {overlap_display} Thursday To Tuesday Wednesday Week day Week day hours Week day time range Week days hours Week days time ranges undefined Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Fermé Heure de fermeture Motif de fermeture Le motif de fermeture n'est utilisé que si le champ "Fermé" est coché Vendredi Du Jours fériés Jours fériés Tranches horaires des jours fériés Si coché, prévaut sur les valeurs des plages horaires Si coché, prévaut sur les valeurs journalières Lundi Aucun horaire d'ouverture défini Ouvert Ouvert 24/7 Horaires d'ouverture Gestion des horaires d'ouverture Heure d'ouverture Horaires d'ouvertures Fermeture de la popup ... Samedi Période horaire spécifique Tranche horaire d'une période spécifique Périodes exceptionnelles Périodes horaires spécifiques Tranches horaires de périodes spécifiques Dimanche Le formulaire ne peut être validé car des périodes sont en conflit L'heure d'ouverture ne peut être postérieure à l'heure de fermeture L'heure de début ne peut être égale à l'heure de fin La date de début ne peut être postérieure à la date de fin Cette période est en conflit avec la période {overlap_display} Jeudi Au Mardi Mercredi Jour de la semaine Horaires journaliers Tranche horaire journalière Horaires journaliers Tranches horaires journalières non-défini 